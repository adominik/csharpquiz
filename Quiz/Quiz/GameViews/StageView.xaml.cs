﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using System.Windows.Threading;

namespace Quiz
{
    public partial class StageView : PhoneApplicationPage
    {
        IsolatedStorageSettings appSettings = IsolatedStorageSettings.ApplicationSettings;
        StageWrapper game;
        GameInfo gameInfo;
        DispatcherTimer timer = new DispatcherTimer();
        int ReamingTime = 5;
        
        public StageView()
        {
            InitializeComponent();

            game = (StageWrapper)appSettings["stageList"];
            gameInfo = (GameInfo)appSettings["gameInfo"];

            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += new EventHandler(timer_Tick);


            //if (gameInfo.GameStarted)
            // {
            // if (game.StageList.Count == gameInfo.Answers.Count)
            // {

            gameInfo.GameStarted = true;
            gameInfo.Answers.Clear();
            Util.RandomizeStages(ref game);

            //   }
            //  }
            // else
            //{
            //    gameInfo.GameStarted = true;
            //     Util.RandomizeStages(ref game);
            // }

            questionTitle.Text = game.StageList[gameInfo.Answers.Count].Question.ToString();
            answerBtn0.Content = game.StageList[gameInfo.Answers.Count].Answers[0].ToString();
            answerBtn1.Content = game.StageList[gameInfo.Answers.Count].Answers[1].ToString();
            answerBtn2.Content = game.StageList[gameInfo.Answers.Count].Answers[2].ToString();
            answerBtn3.Content = game.StageList[gameInfo.Answers.Count].Answers[3].ToString();
            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            timerTb.Text = ReamingTime.ToString();
            if (ReamingTime == 0)
            {
                ReamingTime = 5;
                answerBtnClick(answerBtn0, new RoutedEventArgs());
                //timer.Stop();
                //NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
            }
            ReamingTime--;

        }


        private void answerBtnClick(object sender, RoutedEventArgs e)
        {
            //Leállítjuk a timert hogy ne fusson tovább, feleseleges gondokat okozva
            ReamingTime = 5;
            //Lekérjük a kattintott gomb sorszámát
            var answerIndex = ((Button)sender).Tag;
            //Megvizsgáljuk hogy vége van-e a játéknak
            if (game.StageList.Count == gameInfo.Answers.Count)
            {
                MessageBox.Show("Vége a játéknak");
            }
            else
            {

                gameInfo.Answers.Add(int.Parse(answerIndex.ToString()));
                appSettings.Remove("gameInfo");
                appSettings.Add("gameInfo", gameInfo);
                //NavigationService.Navigate(new Uri("/GameViews/StageView.xaml", UriKind.Relative));
                if (game.StageList.Count == gameInfo.Answers.Count)
                {
                    timer.Stop();
                    NavigationService.Navigate(new Uri("/GameViews/Result.xaml", UriKind.Relative));
                    
                    return;
                }
                questionTitle.Text = game.StageList[gameInfo.Answers.Count].Question.ToString();
                answerBtn0.Content = game.StageList[gameInfo.Answers.Count].Answers[0].ToString();
                answerBtn1.Content = game.StageList[gameInfo.Answers.Count].Answers[1].ToString();
                answerBtn2.Content = game.StageList[gameInfo.Answers.Count].Answers[2].ToString();
                answerBtn3.Content = game.StageList[gameInfo.Answers.Count].Answers[3].ToString();

                //MessageBox.Show("Válaszok száma" + gameInfo.Answers.Count.ToString());
            }
        }

        private void StageViewPage_Loaded(object sender, RoutedEventArgs e)
        {
            // MessageBox.Show("Loaded");
            //Túlcsordulás levédése...


        }

    }
}
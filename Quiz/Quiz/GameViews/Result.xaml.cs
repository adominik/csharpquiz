﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using System.Windows.Media;

namespace Quiz.GameViews
{
    public partial class Result : PhoneApplicationPage
    {
        IsolatedStorageSettings appSettings = IsolatedStorageSettings.ApplicationSettings;
        StageWrapper game;
        GameInfo gameInfo;
        

        public Result()
        {
            
            InitializeComponent();
            game = (StageWrapper)appSettings["stageList"];
            gameInfo = (GameInfo)appSettings["gameInfo"];

            Quiz.Result result = new Quiz.Result();
            Util.CalculateGameResult(ref game,ref gameInfo, ref result);


            for (int i = 0; i < gameInfo.Answers.Count; i++)
            {
                TextBlock tb = (TextBlock)this.FindName("result" + i + "Tb");

                if (gameInfo.Answers[i] == game.StageList[i].CorrectIndex)
                {
                    tb.Foreground = new SolidColorBrush(Colors.Green);
                    tb.Text = "Helyes";
                }
                else
                {
                    tb.Foreground = new SolidColorBrush(Colors.Red);
                    tb.Text = "Hibás";
                }
            }

            scoreHolderTb.Text = result.MaxPoint.ToString();

            if(appSettings.Contains("resultList")){
                ResultWrapper rl = (ResultWrapper)appSettings["resultList"];

                appSettings.Remove("resultList");

                rl.Results.Add(result);
                appSettings.Add("resultList",rl);
            }else{
                ResultWrapper rl = new ResultWrapper();
                rl.Results = new List<Quiz.Result>();
                
                scoreHolderTb.Text = result.MaxPoint.ToString();
                if (rl.Results == null)
                {
                    MessageBox.Show("NULL");
                }
                else
                {
                    rl.Results.Add(result);
                }
                
                appSettings.Add("resultList",rl);
            }

            



        }

        private void bactToMenuBtn_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
        }
    }
}
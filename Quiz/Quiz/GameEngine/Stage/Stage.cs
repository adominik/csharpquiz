﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz
{
    class Stage
    {
        public string Question { get; set; }
        public List<string> Answers { get; set; }
        public int CorrectIndex { get; set; }
      
    }
}

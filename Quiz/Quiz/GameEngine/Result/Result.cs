﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz
{
    class Result
    {
        public int MaxPoint { get; set; }
        public DateTime Date { get; set; }

        public override String ToString()
        {
            return "Date: "+Date.ToString()+" ---- "+" Point: "+MaxPoint.ToString();
        }
    }
}

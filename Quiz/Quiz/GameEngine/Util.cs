﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz
{
    class Util
    {
        public static void RandomizeStages (ref StageWrapper stg){
            for (int i = 0; i < stg.StageList.Count; i++)
            {
                Random r = new Random();
                Stage temp = stg.StageList[i];
                int randomIndex = r.Next(i, stg.StageList.Count);
                stg.StageList[i] = stg.StageList[randomIndex];
                stg.StageList[randomIndex] = temp;
            }
        }

        public static void CalculateGameResult(ref StageWrapper st, ref GameInfo gf, ref Result rs)
        {
            rs.Date = DateTime.Now;
            rs.MaxPoint = 0;

            for(int i = 0; i< st.StageList.Count;i++)
            {
                if (st.StageList[i].CorrectIndex == gf.Answers[i])
                {
                    rs.MaxPoint++;
                }
            }

        }
        
    }
}

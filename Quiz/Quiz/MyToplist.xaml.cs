﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;

namespace Quiz
{
    public partial class MyToplist : PhoneApplicationPage
    {
        IsolatedStorageSettings appSettings = IsolatedStorageSettings.ApplicationSettings;
        public MyToplist()
        {
            InitializeComponent();
            if (appSettings.Contains("resultList"))
            {
                ResultWrapper rl = (ResultWrapper)appSettings["resultList"];

                var result = from elem in rl.Results
                             orderby elem.MaxPoint descending
                             select elem;

                for (int i = 0; i < rl.Results.Count; i++)
                {
                    TextBlock tb = (TextBlock)this.FindName("result" + i);
                    tb.Text = result.ElementAt(i).MaxPoint.ToString()+"-"+result.ElementAt(i).Date.ToShortDateString();
                }

            }
        }

        private void backToFomenu_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
        }
    }
}
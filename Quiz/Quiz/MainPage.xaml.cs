﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Quiz.Resources;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Json;
using System.IO.IsolatedStorage;
using Newtonsoft.Json;

namespace Quiz
{
    public partial class MainPage : PhoneApplicationPage
    {
        IsolatedStorageSettings appSettings = IsolatedStorageSettings.ApplicationSettings;

        public MainPage()
        {
            InitializeComponent();
            if (!appSettings.Contains("gameInfo"))
            {
                GameInfo gameInfo = new GameInfo();
                appSettings.Add("gameInfo",gameInfo);
            }
            else
            {
                GameInfo gameInfo = new GameInfo();
                appSettings["gameInfo"] = gameInfo;
            }
            StageWrapper stageWrapperEntity = new StageWrapper();
            if (!appSettings.Contains("stageList"))
            {

                try
                {
                    var src = Application.GetResourceStream(new Uri("Input/quiz.json", UriKind.Relative)).Stream;
                    StreamReader sr = new StreamReader(src);
                    String content = sr.ReadToEnd();
                    stageWrapperEntity = JsonConvert.DeserializeObject<StageWrapper>(content);
                    appSettings.Add("stageList", stageWrapperEntity);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    Application.Current.Terminate();
                }
            }


            //Kérdések
            /*
            Stage s1 = new Stage();
            s1.Question = "Kerdes 1";
            s1.Answers.Add("Answer 0");
            s1.Answers.Add("Answer 1");
            s1.CorrectIndex = 0;

            Stage s2 = new Stage();
            s2.Question = "Kerdes 2";
            s2.Answers.Add("Answer 2");
            s2.Answers.Add("Answer 3");
            s2.CorrectIndex = 0;

            //Pálya
            StageWrapper w1 = new StageWrapper();
            w1.StageList.Add(s1);
            w1.StageList.Add(s2);
            */


        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            IsolatedStorageSettings appSettings = IsolatedStorageSettings.ApplicationSettings;
            GameInfo gameInfo = (GameInfo)appSettings["gameInfo"];
            NavigationService.Navigate(new Uri("/GameViews/StageView.xaml", UriKind.Relative));
        }

        private void toplista_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MyToplist.xaml", UriKind.Relative));
        }

       


    }
}
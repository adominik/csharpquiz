﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz
{
    class GameInfo
    {
        public bool GameStarted { get; set; }
        public List<int> Answers { get; set; }
        public int Point { get; set; }

        public GameInfo()
        {
            GameStarted = false;
            Answers = new List<int>();
            Point = 0;
        }
    }
}
